---
Title:  Kubernetes DNS debug
Author: Amit Karpe
tags:
- Kubernetes
- kubedns
- coredns
hero: Various Note options samples
path: blob/master/docs
source: dns.md

---

[TOC]

```bash
kubectl logs --namespace=kube-system $(kubectl get pods --namespace=kube-system -l k8s-app=kube-dns -o name | head -1) -c kubedns

kubectl logs --namespace=kube-system $(kubectl get pods --namespace=kube-system -l k8s-app=kube-dns -o name | head -1) -c dnsmasq

kubectl logs --namespace=kube-system $(kubectl get pods --namespace=kube-system -l k8s-app=kube-dns -o name | head -1) -c sidecar

for p in $(kubectl get pods --namespace=kube-system -l k8s-app=coredns -o name); do kubectl logs --namespace=kube-system $p; done
```



```shellsession
[sevone@sdi16m etcd-v3.3.12-linux-amd64]$ kubectl run --rm alpinedemo --restart=Never --image=alpine:3.2 -it -- nslookup nginx
If you don't see a command prompt, try pressing enter.
nslookup: can't resolve 'nginx': Name does not resolve
pod "alpinedemo" deleted
pod default/alpinedemo terminated (Error)
[sevone@sdi16m etcd-v3.3.12-linux-amd64]$ kubectl run --rm alpinedemo --restart=Never --image=alpine:3.9 -it -- nslookup nginx2
If you don't see a command prompt, try pressing enter.
Name:      nginx2
Address 1: 10.99.169.75 nginx2.default.svc.cluster.local
pod "alpinedemo" deleted
[sevone@sdi16m etcd-v3.3.12-linux-amd64]$ kubectl run --rm alpinedemo --restart=Never --image=alpine:3.2 -it -- nslookup nginx2
If you don't see a command prompt, try pressing enter.
nslookup: can't resolve 'nginx2': Name does not resolve
pod "alpinedemo" deleted
pod default/alpinedemo terminated (Error)
[sevone@sdi16m etcd-v3.3.12-linux-amd64]$
```

```shellsession
2019-02-17T17:41:59.678Z [INFO] 10.32.0.9:42505 - 18194 "AAAA IN nginx2.default.svc.cluster.local. udp 50 false 512" NOERROR qr,aa,rd,ra 143 0.000268221s
2019-02-17T17:41:59.678Z [INFO] 10.32.0.9:42505 - 17784 "A IN nginx2.default.svc.cluster.local. udp 50 false 512" NOERROR qr,aa,rd,ra 98 0.000378969s


2019-02-17T17:42:22.591Z [INFO] 10.32.0.9:47828 - 63577 "AAAA IN nginx2. udp 24 false 512" NXDOMAIN qr,rd,ra 100 0.001005187s
2019-02-17T17:42:22.611Z [INFO] 10.32.0.9:47828 - 63073 "A IN nginx2. udp 24 false 512" NXDOMAIN qr,rd,ra 100 0.02071319s

^C
[sevone@sdi16m exam]$ kubectl -n kube-system logs coredns-86c58d9df4-rgtpm  -f | grep nginx2
Log enabled - Check for Errors in the DNS pod
[sevone@sdi16m exam]$ kubectl -n kube-system get configmaps coredns -o yaml
apiVersion: v1
data:
  Corefile: ".:53 {\n    errors\n    log\n    health\n    kubernetes cluster.local
    in-addr.arpa ip6.arpa {\n       pods insecure\n       upstream \n       fallthrough
    in-addr.arpa ip6.arpa\n    }\n    prometheus :9153\n    proxy . /etc/resolv.conf\n
    \   cache 30\n    loop\n    reload\n    loadbalance\n}\n"
kind: ConfigMap
metadata:
  creationTimestamp: "2019-02-11T13:01:55Z"
  name: coredns
  namespace: kube-system
  resourceVersion: "692890"
  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
  uid: 357d175a-2dfd-11e9-a79e-0050568c1aa1
[sevone@sdi16m exam]$ kubectl -n kube-system get configmaps coredns -o yaml  | grep log
  Corefile: ".:53 {\n    errors\n    log\n    health\n    kubernetes cluster.local
[sevone@sdi16m exam]$
```
